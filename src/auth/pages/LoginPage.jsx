import React, { useEffect } from 'react'
import './LoginPage.css'
import { useAuthStore, useForm } from '../../hooks'
import Swal from 'sweetalert2'

const loginFormFileds = {
  loginEmail: '',
  loginPassword: ''
}

const registerFormFields = {
  registerEmail: '',
  registerName: '',
  registerPassword: '',
  registerPassword2: '',
}

export const LoginPage = () => {

  const { startLogin, startRegister, errorMessage } = useAuthStore();

  //* Login
  const { loginEmail, loginPassword, onInputChange: onLoginInputChange } = useForm(loginFormFileds);
  const loginSubmit = (event) => {
    event.preventDefault();
    startLogin({ email: loginEmail, password: loginPassword });

  }
  //* Registro
  const { registerEmail, registerName, registerPassword, registerPassword2, onInputChange: onRegisterInputChange } = useForm(registerFormFields);
  const registerSubmit = (event) => {
    event.preventDefault();
    //* Validar contraseñas
    if (registerPassword !== registerPassword2) {
      Swal.fire('Erroe en Registros', 'Contraseñas no son iguales.', 'error');
    }else{
      //* Metodo de Registro
      startRegister({ name: registerName, email: registerEmail, password: registerPassword });
    }
  }

  //* Mensajes Swal
  useEffect(() => {
    if (errorMessage !== undefined) {
      Swal.fire('Error en la Authenticación.', errorMessage, 'error');
    }
  }, [errorMessage])



  return (
    <div className="container login-container">
      <div className="row">
        <div className="col-md-6 login-form-1">
          <h3>Ingreso</h3>
          <form onSubmit={loginSubmit}>
            <div className="form-group mb-2">
              <input
                type="text"
                className="form-control"
                placeholder="Correo"
                name='loginEmail'
                value={loginEmail}
                onChange={onLoginInputChange}
              />
            </div>
            <div className="form-group mb-2">
              <input
                type="password"
                className="form-control"
                placeholder="Contraseña"
                name='loginPassword'
                value={loginPassword}
                onChange={onLoginInputChange}
              />
            </div>
            <div className="d-grid">
              <input
                type="submit"
                className="btnSubmit"
                value="Login"
              />
            </div>
          </form>
        </div>

        <div className="col-md-6 login-form-2">
          <h3>Registro</h3>
          <form onSubmit={registerSubmit}>
            <div className="form-group mb-2">
              <input
                type="text"
                className="form-control"
                placeholder="Nombre"
                name='registerName'
                value={registerName}
                onChange={onRegisterInputChange}
              />
            </div>
            <div className="form-group mb-2">
              <input
                type="email"
                className="form-control"
                placeholder="Correo"
                name='registerEmail'
                value={registerEmail}
                onChange={onRegisterInputChange}
              />
            </div>
            <div className="form-group mb-2">
              <input
                type="password"
                className="form-control"
                placeholder="Contraseña"
                name='registerPassword'
                value={registerPassword}
                onChange={onRegisterInputChange}
              />
            </div>

            <div className="form-group mb-2">
              <input
                type="password"
                className="form-control"
                placeholder="Repita la contraseña"
                name='registerPassword2'
                value={registerPassword2}
                onChange={onRegisterInputChange}
              />
            </div>

            <div className="d-grid">
              <input
                type="submit"
                className="btnSubmit"
                value="Crear cuenta" />
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}
