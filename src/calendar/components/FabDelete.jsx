import React from 'react'
import { useCalendarStore } from '../../hooks'
import { useSelector } from 'react-redux';

export const FabDelete = () => {

    const { startDeleteEvent, hasEventSelected } = useCalendarStore(state => state.calendar);

    const handleDelete = () => {
        startDeleteEvent();
    }

    return (
        <button
            onClick={handleDelete}
            style={{
                display: (hasEventSelected)
                    ? ''
                    : 'none'
            }}
            className='btn btn-danger fab-danger'>
            <i className='fas fa-trash-alt'></i>
        </button>
    )
}
