import React from 'react'
import { useCalendarStore, useUiStore } from '../../hooks'
import { addHours } from 'date-fns';

export const FabAddNew = () => {

    const { openDateModal } = useUiStore(state => state.ui);
    const { setActiveEvent } = useCalendarStore(state => state.calendar);

    const handleClickNew = () => {
        setActiveEvent({
            title: '',
            notes: '',
            start: new Date(),
            end: addHours(new Date(), 2),
            bgColor: '#FFFFFF',
            user: {
                name: 'Andres Vasquez',
                _id: 123
            }
        });
        openDateModal();
    }

    return (
        <button
            onClick={handleClickNew}
            className='btn btn-primary fab'>
            <i className='fas fa-plus'></i>
        </button>
    )
}
