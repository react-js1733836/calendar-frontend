import { useDispatch, useSelector } from "react-redux"
import { calendarApi } from "../api";
import { checking, clearErrorMessage, onLogin, onLogoutCalendar, onLogut } from "../store";



export const useAuthStore = () => {

    const { status, user, errorMessage } = useSelector(state => state.auth);
    const dispatch = useDispatch();
    //* Login
    const startLogin = async ({ email, password }) => {
        dispatch(checking());
        try {
            const { data } = await calendarApi.post('/auth', { email, password });
            localStorage.setItem('token', data.token);
            localStorage.setItem('token-init-date', new Date().getTime());
            dispatch(onLogin({ name: data.name, uid: data.uid }));
        } catch (error) {
            dispatch(onLogut('Crendeciales Incorrectas'))
            setTimeout(() => {
                dispatch(clearErrorMessage());
            }, 10);
        }
    }
    //* Registro
    const startRegister = async ({ name, email, password }) => {
        dispatch(checking());
        try {
            const { data } = await calendarApi.post('/auth/new', { name, email, password });
            localStorage.setItem('token', data.token);
            localStorage.setItem('token-init-date', new Date().getTime());
            dispatch(onLogin({ name: data.name, uid: data.uid }));
        } catch (error) {
            dispatch(onLogut(error.response.data?.msg || '----'));
            setTimeout(() => {
                dispatch(clearErrorMessage());
            }, 10);
        }
    }

    const checkAuthToken = async () => {
        const token = localStorage.getItem('token');
        if (!token) return dispatch(onLogut());
        try {
            const { data } = await calendarApi.get('/auth/renew');
            localStorage.setItem('token', data.token);
            localStorage.setItem('token-init-date', new Date().getTime());
            dispatch(onLogin({ name: data.name, uid: data.uid }));
        } catch (error) {
            localStorage.clear();
            dispatch(onLogut());
        }
    }

    const startLogout = () => {
        localStorage.clear();
        dispatch(onLogut());
        dispatch(onLogoutCalendar());
    }

    return {
        //* Propiedades
        status,
        user,
        errorMessage,
        //* Metodos
        startLogin,
        startRegister,
        checkAuthToken,
        startLogout

    }
}